# -*- coding: utf-8 -*-

import os, sys
import caffe
import numpy as np
import re
import time


root = os.getcwd()

prototxt = root + '/deploy.prototxt'
prototxt_svd = root + '/deploy_sparse.prototxt'

model = root + '/bvlc_reference_caffenet.caffemodel'
model_svd = root + '/caffenet_svd.caffemodel'


def get_all_layer_parameter(net):
    data = lambda blob: blob.data
    params = [(k, map(data, v)) for k, v in net.params.items()]
    return params


def get_specify_layer_parameter(net, layer_name):
    params = net.params[layer_name]
    weights = params[0].data
    biase = params[1].data
    return weights, biase


def compress_weights(W, l):
    """
    Compress the weight matrix W of an inner product (fully connected) layer
    using truncated SVD.
    Parameters:
    W: N x M weights matrix
    l: number of singular values to retain
    Returns:
    Ul, L: matrices such that W approx Ul*L
    """
    
    U, s, V = np.linalg.svd(W, full_matrices=False)

    Ul = U[:, :l]
    sl = s[:l]
    Vl = V[:l, :]

    L = np.dot(np.diag(sl), Vl)
    return Ul, L


def model_accuracy_verification(net, model_id, file_name):
	# load input and configure preprocessing
	transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
	transformer.set_mean('data', np.load('/opt/caffe/python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1))
	transformer.set_transpose('data', (2,0,1))
	transformer.set_channel_swap('data', (2,1,0))
	transformer.set_raw_scale('data', 255.0)
	
	with open(file_name) as fs:
		fcontent = fs.readlines()
		lcount = 0
		vSum = 0
		vAverage = 0
		vlist = []
		for fn in fcontent:
			filepath = "/home/emlaas/data/emlaas/" + model_id + "/images/" + fn.rstrip('\n')
			# if os.path.exists(filepath):
			if os.path.isfile(filepath):
				try:
					#print filepath
					lcount += 1
					#load the image in the data layer
					im = caffe.io.load_image(filepath)
					net.blobs['data'].data[...] = transformer.preprocess('data', im)
					#compute
					out = net.forward()
					vlist.append(out['prob'].argmax())
					#predicted predicted class
					vSum += out['prob'][0][out['prob'].argmax()]
				except:
					print "Unable to open file"
		# print lcount, vSum
		# print vlist
		return vSum/lcount


def simply_accuracy_verification(net, model_id, lcount):
	# load input and configure preprocessing
	transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
	transformer.set_mean('data', np.load('/opt/caffe/python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1))
	transformer.set_transpose('data', (2,0,1))
	transformer.set_channel_swap('data', (2,1,0))
	transformer.set_raw_scale('data', 255.0)

	vSum = 0
	tSum = 0
	filepath = "/home/emlaas/models/emlaas/" + model_id + "/image_verify.jpg"
	print filepath
	#load the image in the data layer
	im = caffe.io.load_image(filepath)
	#compute
	start_time = time.time()
	for i in range(0, lcount):
		net.blobs['data'].data[...] = transformer.preprocess('data', im)
		each_start = time.time()
		out = net.forward()
		#predicted predicted class
		tSum += (time.time() - each_start)
		vSum += out['prob'][0][out['prob'].argmax()]

	print (" --- total verification %f seconds ---" % (time.time() - start_time))
	print (" --- compute %f seconds ---" % (tSum))
	return vSum/lcount



def model_svd_sparsification(net):
	net_svd = caffe.Net(prototxt_svd, model, caffe.TEST)
	# fc6
	l_fc6 = net_svd.params['fc6_L'][0].data.shape[0]
	W_fc6, B_fc6 = get_specify_layer_parameter(net, 'fc6')
	Ul_fc6, L_fc6 = compress_weights(W_fc6, l_fc6)
	
	# install compressed matrix factors (and original biases)
	net_svd.params['fc6_L'][0].data[...] = L_fc6
	net_svd.params['fc6_U'][0].data[...] = Ul_fc6
	net_svd.params['fc6_U'][1].data[...] = B_fc6
	
	# fc7
	l_fc7 = net_svd.params['fc7_L'][0].data.shape[0]
	W_fc7, B_fc7 = get_specify_layer_parameter(net, 'fc7')
	Ul_fc7, L_fc7 = compress_weights(W_fc7, l_fc7)
	
	# install compressed matrix factors (and original biases)
	net_svd.params['fc7_L'][0].data[...] = L_fc7
	net_svd.params['fc7_U'][0].data[...] = Ul_fc7
	net_svd.params['fc7_U'][1].data[...] = B_fc7
	
	print('Ul_fc6.shape', Ul_fc6.shape)
	print('L_fc6.shape', L_fc6.shape)
	print('Ul_fc7.shape', Ul_fc7.shape)
	print('L_fc7.shape', L_fc7.shape)
	
	net_svd.save(model_svd)
	return 'ok'

def replace_string_in_file(sparse_k):
	with open(prototxt_svd, "r") as sources:
		data = sources.read()
		data = re.sub(r"[0-9]*  #fc6_weight_k" , str(sparse_k) + "  #fc6_weight_k" , data)
		data = re.sub(r"[0-9]*  #fc7_weight_k" , str(sparse_k) + "  #fc7_weight_k" , data)
	with open(prototxt_svd, "w") as f:
		f.write(data)
	return 'ok'


def count_folders_in_directory(model_id):
	folders = len(os.walk("/home/emlaas/data/emlaas/" + model_id + "/images").next()[1])
	return folders




def auto_compress_process(): 

	# caffe.set_mode_gpu()
	# caffe.set_device(0)

	net = caffe.Net(prototxt, model, caffe.TEST)

	# modelId = raw_input("Enter modelId number you want to verify: ")
	_, modelId = os.path.split(os.getcwd())
	# print modelId

	start_time = time.time()
	accuracy = model_accuracy_verification(net, modelId, 'val.txt')
	# accuracy = simply_accuracy_verification(net, modelId, 5000)
	print (" --- verification %s seconds --- " % (time.time() - start_time))
	valfolders = count_folders_in_directory(modelId)

	# print accuracy
	print accuracy

	# Automatically assign k to verify accuracy
	sparse_k = 2048
	accuracy_degradation = int(raw_input("Enter the max degradation in accuracy(%): "))
	accuracy_degradation = accuracy_degradation / (100 * 1.0)
	print accuracy_degradation

	while sparse_k > 0:
		print sparse_k
		replace_string_in_file(sparse_k)
	
		# model SVD
		res = model_svd_sparsification(net)
	
		net_svd = caffe.Net(prototxt_svd, model_svd, caffe.TEST)
		ver_start_time = time.time()
		accuracy_svd = model_accuracy_verification(net_svd, modelId, 'val.txt')
		# accuracy_svd = simply_accuracy_verification(net_svd, modelId, 5000)
		# print(" --- sparseSep verification %s seconds --- " % (time.time() - ver_start_time))
	
		if abs(accuracy - accuracy_svd) >= accuracy_degradation:
			print('sparse_k value', sparse_k)
			print('accuracy(origin, final)', accuracy, accuracy_svd)
			break
		else:
			sparse_k = sparse_k // 2
			print('accuracy_svd', accuracy_svd)
			print('sparse_k change', sparse_k)

		if sparse_k < valfolders:
			break


if __name__ == '__main__':
    auto_compress_process()
