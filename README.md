# README #

## Sparsification ##

### Compression ### 
* compress by using svd method
	* modify model deploy setting: deploy_sparse.prototxt
	* automatically
		* caffe_weights_auto.py
		* compress model size asap under accuracy-loss < 5%

### pylint ###
* pylint configuration
	* pylintrc
* pylint script
	* pylint-detect.sh

### Sinica ###
* Including 2 parts
	* matlab
		* to sperate matrix to mutliple small matrix
	* python
		* run to get model with sparsificaiton
