# coding: utf-8

import os
import sys
import re

root = os.getcwd()

prototxt = root + '/deploy.prototxt'
prototxt_sparse = root + '/deploy_sparse.prototxt'



def replace_fcl_separated_in_file(prototxt_sparse, fc6_k, fc7_k):
	with open(prototxt_sparse, "r") as sources:
		data = sources.read()
		data = re.sub(r"[0-9]* #fc6_k", str(fc6_k) + " #fc6_k", data)
		data = re.sub(r"[0-9]* #fc7_k", str(fc7_k) + " #fc7_k", data)
	with open(prototxt_sparse, "w") as f:
		f.write(data)
	return 'ok'


if __name__ == '__main__':
	replace_fcl_separated_in_file(256, 256)
