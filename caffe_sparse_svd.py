# coding: utf-8

import os
import sys
# import caffe
import numpy
import re
import time

root = os.getcwd()

prototxt = root + '/deploy.prototxt'
prototxt_sparse = root + '/deploy_sparse.prototxt'

model = root + '/bvlc_reference_caffenet.caffemodel'
model_sparse = root + '/caffenet_sparse.caffemodel'


def get_specify_layer_parameter(net, layer_name):
	params = net.params[layer_name]
	weights = params[0].data
	biase = params[1].data
	return weights, biase


def fcl_compress_weights(W, l):
	"""
	Compress the weight matrix W of an inner product (fully connected) layer
	using truncated SVD.
	Parameters:
	W: N x M weights matrix
	l: number of singular values to retain
	Returns:
	Ul, L: matrices such that W approx Ul*L
	"""
	
	U, s, V = numpy.linalg.svd(W, full_matrices=False)
	
	Ul = U[:, :l]
	sl = s[:l]
	Vl = V[:l, :]
	
	L = numpy.dot(numpy.diag(sl), Vl)
	return Ul, L



def replace_fcl_separated_in_file(prototxt_sparse, fc6_k, fc7_k):
	with open(prototxt_sparse, "r") as sources:
		data = sources.read()
		data = re.sub(r"[0-9]* #fc6_k" , str(fc6_k) + " #fc6_k" , data)
		data = re.sub(r"[0-9]* #fc7_k" , str(fc7_k) + " #fc7_k" , data)
	with open(prototxt_sparse, "w") as f:
		f.write(data)
	return 'ok'




def sparse_compress_process():
	import caffe

	# caffe.set_mode_gpu()
	# caffe.set_device(0)

	net = caffe.Net(prototxt, model, caffe.TEST)
	net_sparse = caffe.Net(prototxt_sparse, model, caffe.TEST)

	# k value setting
	fc6_k = 256
	fc7_k = 256

	replace_fcl_separated_in_file(prototxt_sparse, fc6_k, fc7_k)

	# get origin parameters
	W_fc6, B_fc6 = get_specify_layer_parameter(net, 'fc6')
	W_fc7, B_fc7 = get_specify_layer_parameter(net, 'fc7')

	Ul_fc6, L_fc6 = fcl_compress_weights(W_fc6, fc6_k)
	Ul_fc7, L_fc7 = fcl_compress_weights(W_fc7, fc7_k)

	print ('Ul_fc6.shape, L_fc6.shape: ', numpy.shape(Ul_fc6), numpy.shape(L_fc6))
	print ('Ul_fc7.shape, L_fc7.shape: ', numpy.shape(Ul_fc7), numpy.shape(L_fc7))

	# install compressed matrix factors (and original biases)
	net_sparse.params['fc6_L'][0].data[...] = L_fc6
	net_sparse.params['fc6_U'][0].data[...] = Ul_fc6
	net_sparse.params['fc6_U'][1].data[...] = B_fc6

	net_sparse.params['fc7_L'][0].data[...] = L_fc7
	net_sparse.params['fc7_U'][0].data[...] = Ul_fc7
	net_sparse.params['fc7_U'][1].data[...] = B_fc7

	net_sparse.save(model_sparse)


if __name__ == '__main__':
    sparse_compress_process()

