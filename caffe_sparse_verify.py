# coding: utf-8

import os, sys
import numpy
import time


root = os.getcwd()

prototxt = root + '/deploy.prototxt'
prototxt_sparse1 = root + '/deploy_sparse.prototxt'
# prototxt_sparse2 = root + '/deploy_sparse2.prototxt'

model = root + '/bvlc_reference_caffenet.caffemodel'
model_sparse1 = root + '/caffenet_sparse.caffemodel'
# model_sparse2 = root + '/caffenet_sparse2.caffemodel'



def model_accuracy_verification(net, model_id, file_name):
	# load input and configure preprocessing
	transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
	transformer.set_mean('data', numpy.load('/opt/caffe/python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1))
	transformer.set_transpose('data', (2,0,1))
	transformer.set_channel_swap('data', (2,1,0))
	transformer.set_raw_scale('data', 255.0)
	
	with open(file_name) as fs:
		fcontent = fs.readlines()
		count_match = 0
		count_err = 0
		cSum = 0	# compute
		tSum = 0	# transformer
		start_time = time.time()
		for fn in fcontent:
			fdata = fn.split()
			filepath = "/home/emlaas/data/emlaas/" + model_id + "/val/" + fdata[0]
			if os.path.isfile(filepath):
				try:
					# print filepath
					#load the image in the data layer
					each_load = time.time()
					im = caffe.io.load_image(filepath)
					net.blobs['data'].data[...] = transformer.preprocess('data', im)
					tSum += (time.time() - each_load)
					
					#compute
					each_start = time.time()
					out = net.forward()
					cSum += (time.time() - each_start)
					
					if int(out['prob'].argmax()) == int(fdata[1]):
						count_match += 1
						# print "match ",count_match
					else:
						count_err += 1
				except:
					print "Unable to open file"
		
		# print vlist
		print (" --- total verification %f seconds ---" % (time.time() - start_time))
		print (" --- compute %f seconds ---" % (cSum))
		print (" --- transformer %f seconds ---" % (tSum))
		
		return count_match, count_err



def multimodels_accuracy_calculate():
	import caffe

	# caffe.set_mode_gpu()
	# caffe.set_device(0)

	net = caffe.Net(prototxt, model, caffe.TEST)
	net_sparse1 = caffe.Net(prototxt_sparse1, model_sparse1, caffe.TEST)
	# net_sparse2 = caffe.Net(prototxt_sparse2, model_sparse2, caffe.TEST)

	_, modelId = os.path.split(os.getcwd())

	count_match, count_err = model_accuracy_verification(net, modelId, 'val.txt')
	print (" --- origin_accuracy %f --- " % (count_match / float(count_match + count_err)))

	for i in range(1, 2):
		net_name = eval("net_sparse" + str(i))
	
		count_match, count_err = model_accuracy_verification(net_name, modelId, 'val.txt')
		print (" --- svd%d_accuracy %f --- " % (i, (count_match / float(count_match + count_err))))



if __name__ == '__main__':
	multimodels_accuracy_calculate()

