# -*- coding: utf-8 -*-

import os, sys
import caffe
import numpy
import re
import time


root = os.getcwd()

prototxt = root + '/deploy.prototxt'
prototxt_conv = root + '/deplopy_conv.prototxt'

model = root + '/bvlc_reference_caffenet.caffemodel'
model_conv = root + '/caffenet_conv.caffemodel'



def get_all_layer_parameter(net):
    data = lambda blob: blob.data
    params = [(k, map(data, v)) for k, v in net.params.items()]
    return params


def get_specify_layer_parameter(net, layer_name):
    params = net.params[layer_name]
    weights = params[0].data
    biase = params[1].data
    return weights, biase


def conv_compress_weights(W, l):
    """
    Compress the weight matrix W of an inner product (fully connected) layer
    using truncated SVD.
    Parameters:
    W: N x M weights matrix
    l: number of singular values to retain
    Returns:
    V, Fl: matrices such that W approx V*Fl
    """

    U, s, F = numpy.linalg.svd(W, full_matrices=False)

    Ul = U[:, :l]
    sl = s[:l]
    Fl = F[:l, :]

    # L = numpy.dot(numpy.diag(sl), Vl)
    V = numpy.dot(Ul, numpy.diag(sl))
    return V, Fl


def fcl_compress_weights(W, l):
    U, s, V = numpy.linalg.svd(W, full_matrices=False)

    Ul = U[:, :l]
    sl = s[:l]
    Vl = V[:l, :]

    L = numpy.dot(numpy.diag(sl), Vl)
    return Ul, L


def convolution_separation(W_conv, filters, channels, dimensions, separate_k):
	# filters = 96
	# channels = 3
	# dimensions = 11
	transMatrix = numpy.zeros((channels*dimensions, filters*dimensions))

	for m in range(0, filters):
		for n in range(0, channels):
			for i in range(0, dimensions):
				for j in range(0, dimensions):
					transMatrix[n*dimensions+i][m*dimensions+j] = W_conv[m][n][i][j]

	V, H = conv_compress_weights(transMatrix, separate_k)

	V_conv = numpy.zeros((separate_k, channels, dimensions, 1))
	H_conv = numpy.zeros((filters, separate_k, 1, dimensions))
	
	for k in range(0, separate_k):
		for c in range(0, channels):
			for l in range(0, dimensions):
				V_conv[k][c][l][0] = V[c*dimensions + l][k]
	
	# print ('V_conv', V_conv)

	for n in range(0, filters):
		for k in range(0, separate_k):
			for l in range(0, dimensions):
				H_conv[n][k][0][l] = H[k][n*dimensions + l]
	
	# print ('H_conv', H_conv)
	
	return V_conv, H_conv




def model_accuracy_verification(net, model_id, file_name):
	# load input and configure preprocessing
	transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
	transformer.set_mean('data', numpy.load('/opt/caffe/python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1))
	transformer.set_transpose('data', (2,0,1))
	transformer.set_channel_swap('data', (2,1,0))
	transformer.set_raw_scale('data', 255.0)
	
	with open(file_name) as fs:
		fcontent = fs.readlines()
		lcount = 0
		vSum = 0
		vAverage = 0
		vlist = []
		start_time = time.time()
		for fn in fcontent:
			filepath = "/home/emlaas/data/emlaas/" + model_id + "/images/" + fn.rstrip('\n')
			if os.path.isfile(filepath):
				try:
					#print filepath
					lcount += 1
					#load the image in the data layer
					im = caffe.io.load_image(filepath)
					net.blobs['data'].data[...] = transformer.preprocess('data', im)
					#compute
					out = net.forward()
					vlist.append(out['prob'].argmax())
					#predicted predicted class
					vSum += out['prob'][0][out['prob'].argmax()]
				except:
					print "Unable to open file"
		
		# print lcount, vSum
		# print vlist
		print (" --- verification %s seconds ---" % (time.time() - start_time))
		return vSum/lcount


def simply_accuracy_verification(net, model_id, lcount):
	# load input and configure preprocessing
	transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
	transformer.set_mean('data', numpy.load('/opt/caffe/python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1))
	transformer.set_transpose('data', (2,0,1))
	transformer.set_channel_swap('data', (2,1,0))
	transformer.set_raw_scale('data', 255.0)
	
	vSum = 0
	filepath = "/home/emlaas/models/emlaas/" + model_id + "/image_verify.jpg"
	#load the image in the data layer
	im = caffe.io.load_image(filepath)
	#compute
	start_time = time.time()
	for i in range(0, lcount):
		net.blobs['data'].data[...] = transformer.preprocess('data', im)
		out = net.forward()
		#predicted predicted class
		vSum += out['prob'][0][out['prob'].argmax()]
	
	print (" --- verification %s seconds ---" % (time.time() - start_time))
	return vSum/lcount



def convolution_compress_process():

	# caffe.set_mode_gpu()
	# caffe.set_device(0)

	net = caffe.Net(prototxt, model, caffe.TEST)
	_, modelId = os.path.split(os.getcwd())


	net_conv = caffe.Net(prototxt_conv, model, caffe.TEST)

	W_conv1, B_conv1 = get_specify_layer_parameter(net, 'conv1')

	# conv1
	conv1_k = net_conv.params['conv1_v'][0].data.shape[0]
	print ('separate_k_conv1', conv1_k)

	V_conv1, H_conv1 = convolution_separation(W_conv1, 96, 3, 11, conv1_k)
	print ('Vconv1.shape', numpy.shape(V_conv1))
	print ('Hconv1.shape', numpy.shape(H_conv1))

	# install compressed matrix factors (and original biases)
	net_conv.params['conv1_v'][0].data[...] = V_conv1
	net_conv.params['conv1_w'][0].data[...] = H_conv1
	net_conv.params['conv1_w'][1].data[...] = B_conv1

	net_conv.save(model_conv)




if __name__ == '__main__':
	convolution_compress_process()

