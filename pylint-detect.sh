#!/bin/bash

pylint_path="./pylint_testResult"

if [ ! -d $pylint_path ]; then
    mkdir -p $pylint_path
fi

files=$(find . -type f -name "caffe_*.py")

for file in $files;
do
    f=${file##*/}
    name=$(echo $f | cut -d'.' -f1)
    /usr/bin/pylint --disable=all --output-format=html $file &> $pylint_path/pylint_$name.html

done

