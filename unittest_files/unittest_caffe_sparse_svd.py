# coding: utf-8

import os
import sys
import caffe
import unittest

sys.path.insert(0,'..')

from caffe_sparse_svd import get_specify_layer_parameter, fcl_compress_weights, replace_fcl_separated_in_file, sparse_compress_process 

# caffe.set_mode_gpu()
# caffe.set_device(0)

root = os.path.dirname(os.getcwd())

dir_unittest_result = root + '/unittest_testResult'

prototxt = root + '/deploy.prototxt'
prototxt_sparse = root + '/deploy_sparse.prototxt'

model = root + '/bvlc_reference_caffenet.caffemodel'

net = caffe.Net(prototxt, model, caffe.TEST)

class TestSparseMethods(unittest.TestCase):

    """ Test for caffe_sparse_svd.py """
    def test_replace_fcl_separated_in_file(self):
        self.assertTrue(replace_fcl_separated_in_file(prototxt_sparse, 256, 256))


    def test_get_specify_layer_parameter(self):
        self.assertTrue(get_specify_layer_parameter(net, 'fc6'))


    def test_fcl_compress_weights(self):
        W_fc6, B_fc6 = get_specify_layer_parameter(net, 'fc6')
        self.assertTrue(fcl_compress_weights(W_fc6, 256))


if __name__ == '__main__':

    if not os.path.exists(dir_unittest_result):
        os.makedirs(dir_unittest_result)

    log_file = open(dir_unittest_result + '/unittest_caffe_sparse_svd.txt', 'w')

    test_sparse = unittest.TestLoader().loadTestsFromTestCase(TestSparseMethods)
    unittest.TextTestRunner(verbosity=2, stream=log_file).run(test_sparse)

