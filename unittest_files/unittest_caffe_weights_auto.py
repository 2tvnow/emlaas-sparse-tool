# coding: utf-8

import os
import sys
import caffe
import unittest

sys.path.insert(0,'..')

from caffe_weights_auto import simply_accuracy_verification

# caffe.set_mode_gpu()
# caffe.set_device(0)

root = os.path.dirname(os.getcwd())

dir_unittest_result = root + '/unittest_testResult'

prototxt = root + '/deploy.prototxt'
model = root + '/bvlc_reference_caffenet.caffemodel'

net = caffe.Net(prototxt, model, caffe.TEST)


class TestSparseMethods(unittest.TestCase):

    """ Test for caffe_weights_auto.py """
    def test_simply_accuracy_verification(self):
        modelId = "111"
        self.assertTrue(simply_accuracy_verification(net, modelId, 500))


if __name__ == '__main__':

    if not os.path.exists(dir_unittest_result):
        os.makedirs(dir_unittest_result)

    log_file = open(dir_unittest_result + '/unittest_caffe_weights_auto.txt', 'w')

    test_sparse = unittest.TestLoader().loadTestsFromTestCase(TestSparseMethods)
    unittest.TextTestRunner(verbosity=2, stream=log_file).run(test_sparse)

