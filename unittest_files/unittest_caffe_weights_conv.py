# coding: utf-8

import os
import sys
import caffe
import unittest

sys.path.insert(0,'..')

from caffe_weights_conv import get_specify_layer_parameter, convolution_separation 

# caffe.set_mode_gpu()
# caffe.set_device(0)

root = os.path.dirname(os.getcwd())

dir_unittest_result = root + '/unittest_testResult'

prototxt = root + '/deploy.prototxt'
model = root + '/bvlc_reference_caffenet.caffemodel'

net = caffe.Net(prototxt, model, caffe.TEST)

class TestSparseMethods(unittest.TestCase):

    """ Test for caffe_weights_conv.py """
    def test_convolution_separation(self):
        W_conv1, B_conv1 = get_specify_layer_parameter(net, 'conv1')
        self.assertTrue(convolution_separation(W_conv1, 96, 3, 11, 20))



if __name__ == '__main__':

    if not os.path.exists(dir_unittest_result):
        os.makedirs(dir_unittest_result)

    log_file = open(dir_unittest_result + '/unittest_caffe_weights_conv.txt', 'w')

    test_sparse = unittest.TestLoader().loadTestsFromTestCase(TestSparseMethods)
    unittest.TextTestRunner(verbosity=2, stream=log_file).run(test_sparse)

