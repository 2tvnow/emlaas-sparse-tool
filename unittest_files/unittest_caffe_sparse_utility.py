# coding: utf-8

import os
import sys
import unittest

sys.path.insert(0,'..')

from caffe_sparse_utility import replace_fcl_separated_in_file

root = os.path.dirname(os.getcwd())

dir_unittest_result = root + '/unittest_testResult'

prototxt = root + '/deploy.prototxt'
prototxt_sparse = root + '/deploy_sparse.prototxt'


class TestSparseMethods(unittest.TestCase):

    """ Test for caffe_sparse_utility.py """
    def test_replace_fcl_separated_in_file(self):
        self.assertTrue(replace_fcl_separated_in_file(prototxt_sparse, 256, 256))



if __name__ == '__main__':

    if not os.path.exists(dir_unittest_result):
        os.makedirs(dir_unittest_result)

    log_file = open(dir_unittest_result + '/unittest_caffe_sparse_utility.txt', 'w')

    test_sparse = unittest.TestLoader().loadTestsFromTestCase(TestSparseMethods)
    unittest.TextTestRunner(verbosity=2).run(test_sparse)
    unittest.TextTestRunner(verbosity=2, stream=log_file).run(test_sparse)



