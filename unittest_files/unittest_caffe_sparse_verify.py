# coding: utf-8

import os
import sys
import caffe
import unittest

sys.path.insert(0,'..')

from caffe_sparse_verify import model_accuracy_verification

# caffe.set_mode_gpu()
# caffe.set_device(0)

root = os.path.dirname(os.getcwd())

dir_unittest_result = root + '/unittest_testResult'

prototxt = root + '/deploy.prototxt'
model = root + '/bvlc_reference_caffenet.caffemodel'

net = caffe.Net(prototxt, model, caffe.TEST)


class TestSparseMethods(unittest.TestCase):

    """ Test for caffe_sparse_verify.py """
    def test_model_accuracy_verification(self):
        modelId = "111"
        val_file = root + '/val.txt'
        self.assertTrue(model_accuracy_verification(net, modelId, val_file))


if __name__ == '__main__':

    if not os.path.exists(dir_unittest_result):
        os.makedirs(dir_unittest_result)

    log_file = open(dir_unittest_result + '/unittest_caffe_sparse_verify.txt', 'w')

    test_sparse = unittest.TestLoader().loadTestsFromTestCase(TestSparseMethods)
    unittest.TextTestRunner(verbosity=2, stream=log_file).run(test_sparse)

