'''
load train weight 
baseline        99%
Tt-SVD          97%
haar            94%
TT-Haar         72.0%%
無scalar factor 71.4%
有scalar factor 30.1%
'''

from __future__ import print_function
import keras
import sys
import os
from keras.datasets import mnist
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Input
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from mpi4py import MPI
from keras.models import Model, load_model

import h5py
import numpy as np

import matplotlib.pyplot as plt
import scipy.io
import build_core_by_index 
import cupy as cp


num_classes    = 10
img_rows, img_cols = 28, 28
target_weights = []
# the data, shuffled and split between train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()




if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
    x_test  = x_test.reshape(x_test.shape[0]  , 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_test  = x_test.reshape(x_test.shape[0]  , img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test  = x_test.astype('float32')


x_train /= 255
x_test  /= 255

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test  = keras.utils.to_categorical(y_test, num_classes)



intermediate_layer_model = load_model('/home/wade/Documents/TT-haar-devolope/weights/my_model_output3200.h5')

# x_test size is (10000,28,28)
data = x_test[0:1,:,:,:]


intermediate_output = intermediate_layer_model.predict(data)

#print(intermediate_output.shape, intermediate_output[0,0] )

input_test = np.reshape(intermediate_output[0,:],(1,3200))
input_test = np.transpose(input_test)



# allocate memory 
layer6   = np.zeros(4096, dtype=float, order='F')
filename = '/home/wade/Documents/TT-haar-devolope/weights/mnist_y_out_layer6.mat'
layer6   = build_core_by_index.tt_construct_layer6(filename, input_test)
layer6_1 = layer6 
layer6_1 = build_core_by_index.Relu_Function(layer6_1)


print('layer6_1 done')





# allocate memory 
layer7 = np.zeros(1024, dtype=float, order='F')

# target_weights[7][0] size is (4096,1024), feacture size is (4096,1), i.e. layer[6]
# need transpose it
# weight_tmp = np.transpose(target_weights[7][0])

# calculate target_weights[7][0]*layer[6] without reconstruct weight

input_test = layer6_1
filename   = '/home/wade/Documents/TT-haar-devolope/weights/mnist_y_out_layer7.mat'
layer7   = build_core_by_index.tt_construct_layer7(filename, input_test)
layer7_1 = layer7 
layer7_1 = build_core_by_index.Relu_Function(layer7_1)


print('layer7_1 done')


# calculate target_weights[8][0]*layer[7] without reconstruct weight
input_test = layer7_1
filename   = '/home/wade/Documents/TT-haar-devolope/weights/mnist_y_out_layer8.mat'

layer8     = build_core_by_index.tt_construct_layer8(filename, input_test)
layer8_1   = layer8 



out = build_core_by_index.softmax(layer8_1)
pred_ans = out
print(out)

print(pred_ans)
print(y_test[0])



