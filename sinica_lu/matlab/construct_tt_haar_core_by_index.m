function [cr, core] = construct_tt_haar_core_by_index(fielname)
    %
    % construct TT-Haat-core by index and column length
    % 
    a = load(fielname);
    
    Core_Index      = a.TH_param.Core_Index;
    HaarMatrixWidth = a.TH_param.HaarMatrixWidth;
    mat_ranks       = a.TH_param.mat_ranks;
    n               = a.TH_param.n;
    
    core    = [];
    ind_pos = 1;
    cor_pos = 1;
    dim     = length(n);

    r    = [1 ;mat_ranks' ;1];

    for i = 1 : dim-1 
        column_length = HaarMatrixWidth(i);
        
        H = ConstructHaarWaveletTransformationMatrix(column_length);

        r1 = mat_ranks(i);      
        k  = r1;
        u  = H';
        M = n(i)*r(i); 
        

        index   = Core_Index(ind_pos:ind_pos + k - 1);
        ind_pos = ind_pos + k;

        


        new_index = sort(index);
        
        u = u(:,new_index) ;

        if column_length ~= M
            u = u(1:M,:);
        end
        
        
        core(cor_pos:cor_pos+numel(u)-1, 1) = u(:);
        cor_pos = cor_pos + numel(u);

    end 

    last_core = a.TH_param.last_core;
    core(cor_pos:cor_pos+numel(last_core)-1)=last_core;   
    
    
    

    ps=cumsum([1;n.*r(1:dim).*r(2:dim+1)]);
    
    tt.d = dim; tt.n = n; tt.ps = ps; tt.core = core; tt.r = r;
    [cr] = construct_TT_Haar_core(tt);
    
end


