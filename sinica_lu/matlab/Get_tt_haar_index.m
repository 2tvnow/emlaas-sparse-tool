function [Core_Index, HaarMatrixWidth, last_core, n] = Get_tt_haar_index(varargin)
% This function output the index we want 
% for more detail, can read algorithm 1 step 8.
% r_k indices with the first r_k largest energies, which form a set R
%
if is_array(varargin{1})
    b      = varargin{1};
    n      = size(b);
    n      = n(:);
    d      = numel(n);
    r      = ones(d+1,1);
    rank11 = varargin{2};
    c      = b;
    index_pos       = 1;
    Core_Index      = [];
    HaarMatrixWidth = zeros(d-1,1);
    IsBinary        = varargin{3};
    for i=1:d-1 
        M = n(i)*r(i); 
        rest = numel(c)/M;
        c=reshape(c,[M,rest]);
      
        column_length = M;
        Level         = log2(column_length);

        if 2^fix(Level) < column_length
            Level = ceil(Level);
            column_length = 2^Level; 
            tmp_matrix = zeros(column_length-M, rest);
            c = vertcat(c, tmp_matrix);
        end
        
        H = ConstructHaarWaveletTransformationMatrix(column_length);
        HaarMatrixWidth(i) = column_length;
        
        norm_row_c = zeros(column_length,1);       
        r1=rank11(i);       
            
        k = r1;
        v = H*c;

        for j=1:column_length
            norm_row_c(j)=norm(v(j,:));
        end
        [~, index]=sort(norm_row_c,'descend');
        v(index(k+1:end),:) = [];
        
        r(i+1)=r1;
        
     
        Core_Index(index_pos:index_pos + k - 1) = index(1:k);
        index_pos = index_pos + k;
      
        
      
        
        
        c=v; 
 
        
    end

    if IsBinary 
        last_core = sign(c(:));       
    else    
        last_core = c(:);
    end
   
    
    return;
end