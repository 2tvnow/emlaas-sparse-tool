clearvars;clc;
%%set param
% *************************************************************************
% *************************************************************************
% This is the ui of tt-haar decomp. Input is the training weight matrix          
% output is tt-haar core, saving in t1                                     
% A is a size MxN weight matrix.
% M : m_1 * m_2 * ... * m_d, left_modes  = [m_1, m_2, ..., m_d] 
% N : n_1 * n_2 * ... * n_d, right_modes = [n_1, n_2, ..., n_d]
% Assume G_i is tt-haar core, where i = 1...d
% A = G_1 * ... * G_d, G_i size is q_k-1 * r_k * q_k, where q_k = m_k * n_k
% mat_ranks = [r_1, ... , r_k]
% *************************************************************************
% *************************************************************************
filename = '../weights/mnist_weights_2_23.h5';
left_modes  = [4 4 4 16 ];
right_modes = [4 4 16 16];
mat_ranks   = [16 16 256];
IsBinary    = 1;
%%


hinfo  = hdf5info(filename);
dset   = hdf5read(hinfo.GroupHierarchy.Groups(5).Groups.Datasets(2));
bias   = hdf5read(hinfo.GroupHierarchy.Groups(5).Groups.Datasets(1));
weight = double(dset);

% reshape weight weight matrix to hign dimensional tensor
weight = Construct_TT_Format(weight, left_modes, right_modes);


% t1 = tt_haar_tensor(weight, mat_ranks, IsBinary);
[Core_Index, HaarMatrixWidth, last_core, n] = Get_tt_haar_index(weight, mat_ranks, IsBinary);


%%


TH_param.Core_Index        = Core_Index;
TH_param.HaarMatrixWidth   = HaarMatrixWidth;
TH_param.mat_ranks         = mat_ranks;
TH_param.last_core         = last_core;
TH_param.bias              = bias;
TH_param.n                 = n;


% only save index 
save('~/Documents/TT-haar-devolope/weights/mnist_layer7_param.mat', 'TH_param')


%%
% construct tt haar core by index, and decide last core is binary or not
% 
fielname = '~/Documents/TT-haar-devolope/weights/mnist_layer7_param.mat';

[cr, core] = construct_tt_haar_core_by_index(fielname);
r                  = [1 ; mat_ranks' ; 1];

TT_mat.tensor      = cr.tensor;
TT_mat.d           = length(right_modes); 
TT_mat.left_modes  = left_modes;
TT_mat.right_modes = right_modes;
TT_mat.bias        = bias;
TT_mat.r           = r;

save('~/Documents/TT-haar-devolope/weights/mnist_y_out_layer7.mat', 'TT_mat')
