function [TT_format_weight] = Construct_TT_Format(weight, left_modes, right_modes)
    
    tmp = [left_modes right_modes];
    
    if length(left_modes) > 2
    
        tmp_weight       = reshape(weight, tmp);
        TT_format_weight = reshape(tmp_weight, left_modes.*right_modes);
    else 
        TT_format_weight = reshape(weight, tmp);
    end   
end