clearvars;clc;
%%set param
filename = '../weights/mnist_weights_2_23.h5';
left_modes  = [10];
right_modes = [1024];
mat_ranks   = [16];
IsBinary    = 1;
%%


hinfo = hdf5info(filename);
dset  = hdf5read(hinfo.GroupHierarchy.Groups(6).Groups.Datasets(2));
bias  = hdf5read(hinfo.GroupHierarchy.Groups(6).Groups.Datasets(1));

weight = double(dset);
weight = Construct_TT_Format(weight, left_modes, right_modes);


% t1 = tt_haar_tensor(weight, mat_ranks, IsBinary);
[Core_Index, HaarMatrixWidth, last_core, n] = Get_tt_haar_index(weight, mat_ranks, IsBinary);


TH_param.Core_Index        = Core_Index;
TH_param.HaarMatrixWidth   = HaarMatrixWidth;
TH_param.mat_ranks         = mat_ranks;
TH_param.last_core         = last_core;
TH_param.bias              = bias;
TH_param.n                 = n;



save('~/Documents/TT-haar-devolope/weights/mnist_layer8_param.mat', 'TH_param')

%%
fielname = '~/Documents/TT-haar-devolope/weights/mnist_layer8_param.mat';
[cr, core] = construct_tt_haar_core_by_index(fielname);
r                  = [1 ; mat_ranks' ; 1];

TT_mat.tensor      = cr.tensor;
TT_mat.d           = length(right_modes); 
TT_mat.left_modes  = left_modes;
TT_mat.right_modes = right_modes;
TT_mat.bias        = bias;
TT_mat.r           = r;


save('~/Documents/TT-haar-devolope/weights/mnist_y_out_layer8.mat', 'TT_mat')