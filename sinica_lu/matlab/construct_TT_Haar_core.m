function [cr] = construct_TT_Haar_core(tt)

    d=tt.d; n=tt.n; ps=tt.ps; core=tt.core; r=tt.r;
      
    for i=1:d
        cr.tensor{i}=core(ps(i):ps(i+1)-1);
        cr.tensor{i}=reshape(cr.tensor{i},[r(i),n(i),r(i+1)]);
    end
   

end
    


    
