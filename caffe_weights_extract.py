# coding: utf-8

import os, sys
import caffe
import numpy


root = os.getcwd()

prototxt = root + '/deploy.prototxt'
model = root + '/bvlc_reference_caffenet.caffemodel'

"""
def get_all_layer_parameter(net):
    data = lambda blob: blob.data
    params = [(k, map(data, v)) for k, v in net.params.items()]
    return params
"""

def get_specify_layer_parameter(net, layer_name):
    params = net.params[layer_name]
    weights = params[0].data
    biase = params[1].data
    return weights, biase



def layer_extract_process():

    caffe.set_mode_gpu()
    caffe.set_device(0)

    net = caffe.Net(prototxt, model, caffe.TEST)

    W_conv, B_conv = get_specify_layer_parameter(net, 'conv1')
    print('W_conv.shape, W_conv.size: ', W_conv.shape, W_conv.size)


if __name__ == '__main__':
    layer_extract_process()

